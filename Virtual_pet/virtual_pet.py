class virtual_pet(object):
    def __init__(self):
        self.hungriness = 50
        self.fullness = 50
        self.happiness = 0
        self.tiredness = 0
    
    @property
    def get_hungriness(self):
        return self.hungriness

    @property
    def get_fullness(self):
        return self.fullness

    def feed(self):
        self.hungriness -= 10
        self.fullness += 10
        return self.hungriness, self.fullness
    
    def play(self):
        self.happiness += 10
        self.tiredness += 10
        return self.happiness, self.tiredness
    
    def sleep(self):
        self.tiredness = 0
        return self.tiredness
    
    def poop(self):
        self.fullness -= 10
        return self.fullness
        